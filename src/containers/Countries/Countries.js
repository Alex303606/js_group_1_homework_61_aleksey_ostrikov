import React, {Component} from 'react';
import './Countries.css';
import CounriesList from "../../components/CountriesList/CountriesList";
import CountryInfo from "../../components/CountryInfo/CountryInfo";
import axios from 'axios'

class Countries extends Component {
	
	state = {
		counries: [],
		country: {}
	};
	
	getCountryInfo = async (code) => {
		let info = await axios.get(`v2/alpha/${code}`);
		this.setState({country: info.data});
	};
	
	async componentDidMount() {
		try {
			let counriesList = await axios.get('v2/all?fields=name;alpha3Code');
			this.setState({counries: counriesList.data});
		} catch (error) {
			console.log('Ошибка сети', error);
		}
	}
	
	render() {
		return (
			<div className="Countries">
				<CounriesList countryInfo={this.getCountryInfo} coutries={this.state.counries}/>
				<CountryInfo info={this.state.country}/>
			</div>
		)
	}
}

export default Countries;