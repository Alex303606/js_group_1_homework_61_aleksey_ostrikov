import React from 'react'

const Country = props => {
	return <li onClick={props.info} className="Country">{props.name}</li>
};

export default Country;