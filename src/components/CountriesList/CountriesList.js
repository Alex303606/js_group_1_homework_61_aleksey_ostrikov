import React from 'react'
import Country from "./Country/Country";

const CounriesList = props => {
	return (
		<ul className="CounriesList">
			{
				props.coutries.map(country => {
					return <Country
						key={country.alpha3Code}
						name={country.name}
						info={() => props.countryInfo(country.alpha3Code)}
					/>
				})
			}
		</ul>
	)
};

export default CounriesList;