import React, {Component} from 'react'
import axios from 'axios'

class CountryInfo extends Component {
	state = {
		bordersNames: ['Нет границ'],
		id: null
	};
	
	componentDidUpdate() {
		if (this.props.info.borders) {
			if(!this.state.id ||
				(this.state.id && this.state.id !== this.props.info.alpha3Code)) {
				let promises = this.props.info.borders.map(async border => {
					try {
						let info = await axios.get(`v2/alpha/${border}`);
						return info.data.name;
					} catch (error) {
						console.log(error);
					}
				});
				let id = this.state.id;
				id = this.props.info.alpha3Code;
				Promise.all(promises).then(results => {
					let bordersNames = this.state.bordersNames;
					if(results.length !== 0) {
						bordersNames = results;
					} else {
						bordersNames = ['Нет границ'];
					}
					this.setState({bordersNames,id});
				});
			}
		}
	}
	
	render() {
		if (Object.keys(this.props.info).length !== 0) {
			return (
				<div className="CountryInfo">
					<h2>{this.props.info.name}</h2>
					<img width={'200'} src={this.props.info.flag} alt="flag"/>
					<div className="CountryInfo__capital">Столица: {this.props.info.capital}</div>
					<h3>Границы:</h3>
					<ul>
						{this.state.bordersNames.map(name => <li key={name}>{name}</li>)}
					</ul>
				</div>
			)
		}
		return <h1>Выберите страну</h1>;
	}
}

export default CountryInfo;